# Calibration_Fail_Counter

How to use:

1. Set INFILENAME field to the name of the calibration data csv file placed in the same folder as the script.
2. Set FAILSEARCH field to the letter corresponding to the desired fail reason to search devices for.
3. Run the script. The bar chart will diplay the occurence of each fail reason within the calibration data, and the devices corresponding to the specified fail reason will be listen in the terminal.
